--
-- File generated with SQLiteStudio v3.4.4 on Sun Sep 24 21:31:57 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    catagory_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         catagory_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'coffee'
                     );

INSERT INTO category (
                         catagory_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'dessert'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name        TEXT (50) UNIQUE,
    product_price       REAL      NOT NULL,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    category_id         INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES category (catagory_id) ON DELETE RESTRICT
                                                                    ON UPDATE RESTRICT
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        1,
                        'Espresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        3,
                        'Cake Ciffon',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        4,
                        'Butter Cake',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    user_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    user_name     TEXT (50) UNIQUE,
    user_gender   TEXT (3)  NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_role     INTEGER   NOT NULL
);

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     1,
                     'paweena',
                     'F',
                     'password',
                     1
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     2,
                     'user1',
                     'M',
                     'password',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     3,
                     'user2',
                     'F',
                     'password',
                     2
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
